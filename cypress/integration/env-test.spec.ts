import { Environments } from '../support/function';

const getUrl = new Environments().getEnv();

describe('Testing Environment Configurations ' + getUrl, () => {

    it('should be returning the correct env', () => {
        cy.visit(getUrl);
    });
});
